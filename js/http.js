function httpExecute(category) {
    var req = new XMLHttpRequest();
    var URL = "https://services.odata.org/V4/Northwind/Northwind.svc/" + category
    req.open('GET', URL, false);
    req.send(null);
    if (req.status == 200) {
        console.log(req)
        console.log(req.responseText);
    } else {
        alert("Error with URL");
    }
}

function httpReq(category) {
    var URL = "https://services.odata.org/V4/Northwind/Northwind.svc/" + category
    var req = new XMLHttpRequest();
    req.onreadystatechange = function () {
        if (this.status == 200 && this.readyState == 4) {
            if(category == "Products"){
                processResponse(JSON.parse(this.responseText), ["ProductName","UnitPrice","UnitsInStock"])
            }else if (category == "Customers"){
                processResponse(JSON.parse(this.responseText), ["ContactName","City","Country"])
            }
        }
    }
    req.open('GET', URL, false);
    req.send();
}

function processResponse(JSONProductos, params) {
    var flagURL = "https://www.countries-ofthe-world.com/flags-normal/flag-of-"
    var divTabla = document.getElementById("divTabla");
    var tabla = document.createElement("table");
    var tbody = document.createElement("tbody");
    tabla.classList.add("table");
    tabla.classList.add("table-striped");
    var img;
    for (var i = 0; i < JSONProductos.value.length; i++) {
        var nuevaFila = document.createElement("tr");
        var c_1 = document.createElement("td");
        c_1.innerText = JSONProductos.value[i][params[0]];
        var c_2 = document.createElement("td");
        c_2.innerText = JSONProductos.value[i][params[1]];
        var c_3 = document.createElement("td");
        if(params[2]=="Country"){
            img = document.createElement("img");
            img.classList.add("flag");
            if(JSONProductos.value[i].Country == "UK"){
                img.src=flagURL+"United-Kingdom.png"
            }else{
                img.src=flagURL+JSONProductos.value[i].Country+".png"
            }
            c_3.appendChild(img)
            tabla.id = "customers_table"
            !!document.getElementById("products_table") ? document.getElementById("products_table").parentNode.removeChild(document.getElementById("products_table")): ""
        }else{
            c_3.innerText = JSONProductos.value[i][params[2]];
            tabla.id = "products_table"
            !!document.getElementById("customers_table") ? document.getElementById("customers_table").parentNode.removeChild(document.getElementById("customers_table")): ""
        }
        nuevaFila.appendChild(c_1);
        nuevaFila.appendChild(c_2);
        nuevaFila.appendChild(c_3);
        tbody.appendChild(nuevaFila);
    }
    tabla.appendChild(tbody);
    divTabla.appendChild(tabla);
}